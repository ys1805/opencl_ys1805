package monteCarloSimulation;

import java.util.List;
import org.apache.commons.math3.util.Pair;
import org.joda.time.DateTime;

public interface PathGenerator {
		
	public List<Pair<DateTime, Double>> getPath();

}

