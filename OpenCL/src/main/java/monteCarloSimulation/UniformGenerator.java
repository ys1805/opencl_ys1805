package monteCarloSimulation;

import java.util.Random;

/**
 * Created by Yishen on 12/11/2014.
 */
public class UniformGenerator  implements RandomVectorGenerator {

    private Random random = new Random();
    private int size;

    public UniformGenerator(int n) {
        this.size = n;
    }

    @Override
    public double[] getVector() {

        // We will generate a vector of normal distributed numbers for the update of stock prices.
        double[] num = new double[this.size];
        for (int i=0; i<this.size; i++) {
            num[i] = random.nextDouble();
        }
        return num;

    }


}
