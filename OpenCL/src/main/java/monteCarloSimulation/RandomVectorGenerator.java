package monteCarloSimulation;

public interface RandomVectorGenerator {
	
	public double[] getVector();

}
