package monteCarloSimulation;

public interface PayOut {

	public double getPayout(PathGenerator path);
	
}
