package monteCarloSimulation;

import com.nativelibs4java.opencl.*;
import org.bridj.Pointer;

import static org.bridj.Pointer.allocateFloats;

/**
 * Created by Yishen on 12/11/2014.
 */
public class GPUNormalGenerator implements RandomVectorGenerator {

    // Size of the normal random vector
    private int size;
    // Number of batch we needed
    private int batch;
    // Array to store the number we generated
    private double[] seq;
    // We use this to track to which place of the sequence we already get.
    private int idx;

    public GPUNormalGenerator(int n) {

        this.size = n;
        // We use 1024*1024 instead of 2*1024*1024 because when we generate the real batch for number, we generate two together
        this.batch = 1024*1024;
        this.seq = getNormalSeq(this.batch);

    }

    @Override
    // We will generate a vector of normal distributed numbers for the update of stock prices.
    public double[] getVector() {
        double[] vector = new double[this.size];
        for(int i = 0; i < size; i++){
            // if the idx reaches the generated amount of normal random number
            // we regenerate a batch of random number again
            if(idx == 2*batch - 1) {
                seq = getNormalSeq(this.batch);
            }
            // we assign the generated normal to the vector needed for the simulation
            vector[i] = seq[idx];
            idx++;
        }
        return vector;
    }

    // This method will generate normal sequence using GPU.
    // The code is mainly from Prof.Eran.
    public double[] getNormalSeq(int batch) {

        idx = 0;
        // Creating the platform which is out computer.
        CLPlatform clPlatform = JavaCL.listPlatforms()[0];
        // Getting the GPU device
        CLDevice device = clPlatform.getBestDevice();
        // Let's make a context
        CLContext context = JavaCL.createContext(null, device);
        // Lets make a default FIFO queue.
        CLQueue queue = context.createDefaultQueue();

        // Read the program sources and compile them :
        String src = "__kernel void add_floats(__global const float* a, __global const float* b, __global float* out1, __global float* out2, float pi, int n) \n" +
                "{\n" +
                "    int i = get_global_id(0);\n" +
                "    if (i >= n)\n" +
                "        return;\n" +
                "\n" +
                "    out1[i] = sqrt(-2*log(a[i]))*sin(2*pi*b[i]);\n" +
                "    out2[i] = sqrt(-2*log(a[i]))*cos(2*pi*b[i]);\n" +
                "}";
        CLProgram program = context.createProgram(src);
        program.build();

        CLKernel kernel = program.createKernel("add_floats");

        final double[] uniformNumber = (new UniformGenerator(2*batch)).getVector();
        final Pointer<Float>
                aPtr = allocateFloats(batch),
                bPtr = allocateFloats(batch);

        for (int i = 0; i < batch; i++) {
            aPtr.set(i, (float) uniformNumber[2*i]);
            bPtr.set(i, (float) uniformNumber[2*i+1]);
        }

        // Create OpenCL input buffers (using the native memory pointers aPtr and bPtr) :
        CLBuffer<Float>
                a = context.createFloatBuffer(CLMem.Usage.Input, aPtr),
                b = context.createFloatBuffer(CLMem.Usage.Input, bPtr);

        // Create an OpenCL output buffer :
        CLBuffer<Float>
                out1 = context.createFloatBuffer(CLMem.Usage.Output, batch),
                out2 = context.createFloatBuffer(CLMem.Usage.Output, batch);
        kernel.setArgs(a, b, out1, out2, (float) Math.PI, batch);
        CLEvent event = kernel.enqueueNDRange(queue, new int[]{batch}, new int[]{128});
        event.invokeUponCompletion(new Runnable() {
            @Override
            public void run() {

            }
        });
        final Pointer<Float> c1Ptr = out1.read(queue,event);
        final Pointer<Float> c2Ptr = out2.read(queue,event);

        double[] seq = new double[2*batch];
        for (int i=0; i < batch; i++){
            seq[2*i] = c1Ptr.get(i);
            seq[2*i+1] = c2Ptr.get(i);
        }

        return seq;

    }

}
