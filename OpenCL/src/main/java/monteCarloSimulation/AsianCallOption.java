package monteCarloSimulation;

import java.util.Iterator;
import java.util.List;

import org.apache.commons.math3.util.Pair;
import org.joda.time.DateTime;

public class AsianCallOption implements PayOut {

	private double strike;
	
	public AsianCallOption(double strike){
		this.strike = strike;
	}
	
	@Override
	public double getPayout(PathGenerator generator) {
		
		// We use Iterator to go through the whole list.
		// Add up the historical prices and calculate the average, comparing with the strike price.
		List<Pair<DateTime, Double>> path = generator.getPath(); 
		Iterator<Pair<DateTime, Double>> iterator = path.iterator(); 
		double sum = 0.0;
		while (iterator.hasNext()) {
			sum += iterator.next().getSecond();
		}
		double average = sum/path.size();
		return Math.max(average-strike, 0);
	}

}
