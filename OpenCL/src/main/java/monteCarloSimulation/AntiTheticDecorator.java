package monteCarloSimulation;

import java.util.Arrays;

public class AntiTheticDecorator implements RandomVectorGenerator {

	private RandomVectorGenerator rvg;
	double[] vector;
	
	public AntiTheticDecorator(RandomVectorGenerator rvg) {
		this.rvg = rvg;
	}

	@Override
	public double[] getVector() {
		// If the original vector has nothing, we generate a new one.
		if (vector == null) {
			vector = rvg.getVector();
			return vector;
		} else {
			// If this vector has already be generated, we will use decorator to generate one with all the elements being the opposite.
			// Clean the vector after this move.
			double[] temp = Arrays.copyOf(vector, vector.length);
			vector = null;
			for (int i=0; i<temp.length; i++) {
				temp[i] = -temp[i];
			}
			return temp;
		}
	}

}
