package monteCarloSimulation;

import java.util.Random;

public class NormalGenerator implements RandomVectorGenerator {

	private Random normal = new Random();
	private int size;
	
	public NormalGenerator(int n) {
		this.size = n;
	}
	
	@Override
	public double[] getVector() {
		
		// We will generate a vector of normal distributed numbers for the update of stock prices.
		double[] num = new double[this.size];
		for (int i=0; i<this.size; i++) {
			num[i] = normal.nextGaussian();
		}
		return num;
		
	}

}
