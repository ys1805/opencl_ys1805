package monteCarloSimulation;

import org.apache.commons.math3.distribution.NormalDistribution;
import org.joda.time.DateTime;

public class SimulationManager {

	private double rate;
	private double sigma;
	private double initialPrice;
	private int dayCount;
	private DateTime startDate;
	private double strike;
	private double error;
	private RandomVectorGenerator rvg;
	private PayOut style;
	private PathGenerator generator;
	private double zscore;
	int _loop;
	
	public SimulationManager(String type, double initialPrice, double sigma, double rate, int dayCount, DateTime startDate, double strike, double prob, double error) throws Exception {
		
		this.rate = rate;
		this.sigma = sigma;
		this.initialPrice = initialPrice;
		this.dayCount = dayCount;
		this.startDate = startDate;
		this.strike = strike;
		this.error = error;	
		this.rvg = new AntiTheticDecorator(new GPUNormalGenerator(this.dayCount));
		this._loop = 0;
		
		// Determine which kind of option are we going to calculate. Throws Exceptions when it is not specified.
		if (type.equals("European")) {
			this.style = new EuropeanCallOption(this.strike);
		} else if (type.equals("Asian")) {
			this.style = new AsianCallOption(this.strike);
		} else {
			throw new Exception( String.format( "No such kind of option") );
		}
		
		this.generator = null;
		// Calculate the zscore of the confidence level.
		NormalDistribution z = new NormalDistribution(0,1);
		this.zscore = z.inverseCumulativeProbability(1-(1-prob)/2);
		
	}
		
	public double simulate() {
		
		StatsCollector stats = new StatsCollector();
		
		// Keep doing simulation until the error is smaller than the specified level.
		do {
			this.generator = new GBMPathGenerator(rate, sigma, initialPrice, dayCount, startDate, rvg);
			double payoff = this.style.getPayout(this.generator);
			stats.oneMore(payoff);
			this._loop ++;
		} while (stats.getNum()<100 || stats.getNum()<this.zscore*this.zscore*stats.getVariance()/(this.error*this.error));
		// Calculate the discount price of the option.
		double price = stats.getMean()*Math.exp(-this.rate*this.dayCount);
		return price;
		
	}
	
}
