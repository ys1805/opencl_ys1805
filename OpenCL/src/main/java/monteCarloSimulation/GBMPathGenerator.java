package monteCarloSimulation;

import java.util.List;
import java.util.LinkedList;

import org.apache.commons.math3.util.Pair;
import org.joda.time.DateTime;

public class GBMPathGenerator implements PathGenerator {

	private double rate;
	private double sigma;
	private double initialPrice;
	private int dayCount;
	private DateTime startDate;
	private double[] normal;
	
	public GBMPathGenerator(double rate, double sigma, double initialPrice, int dayCount, DateTime startDate, RandomVectorGenerator rvg) {
		
			this.rate = rate;
			this.sigma = sigma;
			this.initialPrice = initialPrice;
			this.dayCount = dayCount;
			this.startDate = startDate;
			this.normal = rvg.getVector();
	}
	
	public double getRate() {
		
		return rate;
		
	}
	
	public int getDayCount() {
		
		return dayCount;
		
	}
	
	public DateTime getDate() {
		
		return startDate;
		
	}
	
	public double[] getNormal() {
		
		return normal;
		
	}
	
	public DateTime nextOpenDay(DateTime date) {
		// Stock market only open on the weekdays, so we will only update the price on the weekdays.
		// So when the date comes to the weekends, we will push to the next Monday.
		date = date.plusDays(1);
		if (date.getDayOfWeek() == 6) {
			date = date.plusDays(2);
		} else if (date.getDayOfWeek() == 7) {
			date = date.plusDays(1);
		}
		return date;
	}
	
	@Override
	public List<Pair<DateTime, Double>> getPath() {
		
		List<Pair<DateTime, Double>> path = new LinkedList< Pair<DateTime, Double>>();
		// We first add the current price with current date to the prices path.
		Pair<DateTime, Double> pair = new Pair<DateTime, Double> (startDate, initialPrice);
		path.add(pair);
		for (int i=0; i<dayCount; i++) {
			DateTime date = nextOpenDay(pair.getFirst());
			double price;
			// The price of the next day will be updated based on the Geometic Brownian Motion.
			price = pair.getSecond()*Math.exp((rate-sigma*sigma/2)+sigma*normal[i]);
			Pair<DateTime, Double> newPair = new Pair<DateTime, Double> (date, price);
			path.add(newPair);
			pair = newPair;			
		}
		return path;
	}

}
