package monteCarloSimulation;

import java.util.List;

import org.apache.commons.math3.util.Pair;
import org.joda.time.DateTime;

public class EuropeanCallOption implements PayOut {

	private double strike;
	
	public EuropeanCallOption(double strike){
		this.strike = strike;
	}
	
	@Override
	public double getPayout(PathGenerator generator) {
		
		// The payoff of this option will be the positive part of the difference between last day stock price with strike.
		List<Pair<DateTime, Double>> path = generator.getPath(); 
		double payoff = Math.max(path.get(path.size()-1).getSecond() - this.strike, 0);
		return payoff;
	
	}

}
