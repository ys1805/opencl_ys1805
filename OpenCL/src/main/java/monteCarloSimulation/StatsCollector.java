package monteCarloSimulation;

public class StatsCollector {

	private double mean;
	private double sumSquare;
	private double variance;
	private int num;
	
	public StatsCollector() {
		
		this.mean = 0;
		this.sumSquare = 0;
		this.variance = 0;
		this.num = 0;
		
	}
	
	public double getMean() {
		
		return mean;
		
	}
	
	public double getVariance() {
		
		return variance;
		
	}
	
	public int getNum() {
		
		return num;
		
	}
	
	public void oneMore(double payoff) {
		
		// Update the mean and variance when a new data was added.
		num += 1;
		mean = (mean*(num-1)+payoff)/num;
		sumSquare += payoff*payoff;
		variance = sumSquare/num-mean*mean;
		
	}
	
}
