package monteCarloSimulation;

import java.util.List;

import org.apache.commons.math3.util.Pair;
import org.joda.time.DateTime;

import monteCarloSimulation.AntiTheticDecorator;
import monteCarloSimulation.NormalGenerator;
import junit.framework.TestCase;

public class Test_GBMPathGenerator extends TestCase {
	
	double rate = 0.0001;
	double sigma = 0.01;
	double initialPrice = 100.0;
	int dayCount = 2;
	DateTime date = new DateTime(2014,10,10,20,42);
	RandomVectorGenerator rvg = new AntiTheticDecorator(new GPUNormalGenerator(dayCount));
	GBMPathGenerator generator = new GBMPathGenerator(rate, sigma, initialPrice, dayCount, date, rvg);
	
	public void testNextOpenDay() {
		
		// We test whether it will get the right next day. Especially before the weekends.
		assertTrue(date.getDayOfWeek() == 5);
		date = generator.nextOpenDay(date);
		assertTrue(generator.getDate().getDayOfWeek() == 5);
		assertTrue(date.getDayOfWeek() == 1);
		date = generator.nextOpenDay(date);
		assertTrue(date.getDayOfWeek() == 2);
		
	}
	
	public void testGetPath() {
		
		// We make up a path and test whether the prices are calculated correctly.
		List<Pair<DateTime, Double>> path = generator.getPath();
		double price = path.get(0).getSecond();
		assertTrue(price == initialPrice);
		price = path.get(1).getSecond();
		assertTrue(price == initialPrice*Math.exp((rate-sigma*sigma/2)+sigma*generator.getNormal()[0]));
		double price_1 = path.get(2).getSecond();
		assertTrue(price_1 == price*Math.exp((rate-sigma*sigma/2)+sigma*generator.getNormal()[1]));
		
	}
	
}
