package monteCarloSimulation;

import org.joda.time.DateTime;

import junit.framework.TestCase;

public class Test_SimulationManager extends TestCase {

	public void testSimulate() throws Exception {
		
		// We test an option. This option should be priced almost equal to 1
		SimulationManager simulation = new SimulationManager("European", 1, 0.0001, 0.0, 500, new DateTime(2014,10,12,23,11), 0, 0.99, 0.01);
		double price = simulation.simulate();
		assertTrue(Math.abs(price-1)<0.01);
		
	}
	
}
