package monteCarloSimulation;

import junit.framework.TestCase;

/**
 * Created by Yishen on 12/12/2014.
 */
public class Test_GPUNormalGenerator extends TestCase {

    public void test_getVector(){
        int n = 1000000;
        int batch = 1024*1024;
        GPUNormalGenerator rvg = new GPUNormalGenerator (n);
        double[] normal = rvg.getVector();
        double tol = 0.01;

        StatsCollector stats = new StatsCollector();
        for(int i = 0; i < n; i++){
            stats.oneMore(normal[i]);
        }

        System.out.println(stats.getMean());
        System.out.println(stats.getVariance());
        assertEquals(0., stats.getMean(), tol);
        assertEquals(1.0, stats.getVariance(), tol);
    }

}
