package monteCarloSimulation;

import junit.framework.TestCase;

public class Test_StatsCollector extends TestCase {

	public void testOneMore() {
		
		// We test whether stats collector calculated right with simple cases.
		StatsCollector stats = new StatsCollector();
		assertTrue(stats.getMean()==0);
		assertTrue(stats.getVariance()==0);
		assertTrue(stats.getNum()==0);
		stats.oneMore(1);
		assertTrue(stats.getMean()==1);
		assertTrue(stats.getVariance()==0);
		assertTrue(stats.getNum()==1);
		stats.oneMore(2);
		assertTrue(stats.getMean()==1.5);
		assertTrue(stats.getVariance()==0.25);
		assertTrue(stats.getNum()==2);
		
	}
	
}
