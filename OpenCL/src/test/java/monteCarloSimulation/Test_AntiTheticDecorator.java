package monteCarloSimulation;

import junit.framework.TestCase;

public class Test_AntiTheticDecorator extends TestCase {

	public void testAntiTheticDecorator() {
		AntiTheticDecorator decorator = new AntiTheticDecorator(new GPUNormalGenerator(100));
		// We first test whether this method can generate a vector with specific length.
		double[] vec1 = decorator.getVector();
		assertTrue(vec1.length == 100);
		double[] vec2 = vec1;
		vec1 = decorator.getVector();
		for (int i=0; i<vec1.length; i++) {
			// We test whether each element when we use this function second time will become its opposite.
			assertTrue(vec1[i] == -vec2[i]);
		}	
	}
	
}
