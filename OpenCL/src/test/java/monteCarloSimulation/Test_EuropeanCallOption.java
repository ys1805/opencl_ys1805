package monteCarloSimulation;

import org.joda.time.DateTime;

import junit.framework.TestCase;

public class Test_EuropeanCallOption extends TestCase {

	public void testEuropeanCallOption() throws Exception {
		
		long start = System.currentTimeMillis();
		// This is the pricing question of European Call Option
		SimulationManager simulation = new SimulationManager("European", 152.35, 0.01, 0.0001, 252, new DateTime(2014,10,12,23,11), 165.0, 0.96, 0.01);
		double price = simulation.simulate();
		// The theoretical price should be around 6.21
		long elapsedTimeMillis = System.currentTimeMillis()-start;
		double elapsedTimeMin = elapsedTimeMillis/(60*1000F);
		System.out.printf("The price of European Call Option by my simulation is: %.4f with %d iterations in %.2f minutes ", price, simulation._loop, elapsedTimeMin);
		assertTrue(Math.abs(price-6.22)<0.01);

	}
	
}
